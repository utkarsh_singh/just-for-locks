package com.justforlocks

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatDelegate
import com.google.firebase.FirebaseApp

class MainApplication : Application() {

    init {
        instance = this
    }

    companion object {
        const val PRIVATE_MODE = 0
        const val PREF_NAME = "com.justforlocks"

        private var instance: MainApplication? = null

        /**
         * Application Context
         */
        fun applicationContext(): Context = instance!!.applicationContext

        /**
         * Application instance
         */
        fun instance() = instance

        /**
         * Initialize Shared Preferences
         */
        fun sharedPreferences(): SharedPreferences =
            applicationContext().getSharedPreferences(PREF_NAME, PRIVATE_MODE)

    }

    /**
     *On create
     */
    override fun onCreate() {
        super.onCreate()
        //Disable night mode
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

        //Initialize Firebase
        initFirebase()
    }

    /**
     * Initialize Firebase
     */
    private fun initFirebase() {
        //Initialize Firebase of Current App
        FirebaseApp.initializeApp(this)
    }
}