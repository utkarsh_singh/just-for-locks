package com.justforlocks

import com.theartofdev.edmodo.cropper.CropImage

const val TOKEN = "token"
const val CROP_IMAGE_REQUEST_CODE = CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE