import com.google.firebase.auth.FirebaseAuth
import com.justforlocks.MainApplication

//Application context
val applicationContext = MainApplication.applicationContext()

//Shared preferences
val SharedPreferences = MainApplication.sharedPreferences()

//Resources
val Resources = applicationContext.resources

//Strings
val stringResource = { resourceId: Int -> Resources.getString(resourceId) }

//Empty
val empty = ""

//User
val User = FirebaseAuth.getInstance().currentUser