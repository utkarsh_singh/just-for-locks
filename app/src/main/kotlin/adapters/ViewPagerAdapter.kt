package adapters

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

/**
 * [ViewPagerAdapter] View Pager Adapter Class
 *
 * @property activity
 * @property count
 */
class ViewPagerAdapter(
    private val activity: AppCompatActivity,
    private val fragments: List<Fragment>
) : FragmentStateAdapter(activity) {

    /**
     * Get Item count in the adapter
     */
    override fun getItemCount(): Int = fragments.size

    /**
     * Crate fragment instance based on position
     *
     * @param position
     */
    override fun createFragment(position: Int): Fragment = fragments[position]
}