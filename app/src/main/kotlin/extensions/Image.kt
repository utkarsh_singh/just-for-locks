package extensions

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import utilities.compressImage

import android.net.Uri
import android.widget.ImageView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.request.transition.DrawableCrossFadeTransition
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import com.justforlocks.R
import empty
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.FileInputStream

//Pick Image
fun Activity.pickImage(aspectRatioX: Int = 1, aspectRatioY: Int = 1) = CropImage.activity()
    .setGuidelines(CropImageView.Guidelines.ON)
    .setAspectRatio(aspectRatioX, aspectRatioY)
    .start(this)

//Compress the image returned by {CropImageLibrary}
private fun Intent.compressImage(quality: Int = 80) = CropImage.getActivityResult(this).uri.compressImage(quality)

/**
 * Upload Image
 *
 * @param uploadDestination
 */
suspend fun Intent.uploadImage(uploadDestination: String , imageQuality : Int = 80) = withContext(Dispatchers.IO) {
    //Get the intent and compress the image and make a file input stream
    val inputStream = FileInputStream(this@uploadImage.compressImage(imageQuality))

    //Storage reference of stored image
    val filepath = FirebaseStorage.getInstance().reference.child(uploadDestination)

    //Size of image uploaded
    val totalSize = inputStream.available()

    //Upload task
    val uploadTask = filepath.putStream(inputStream)

    var uri: Uri? = Uri.EMPTY
    try {
        //Upload image
        Tasks.await(uploadTask)

        //Get Download Url
        uri = getDownloadUrl(uploadTask, filepath)

//            //Upload progress
//            uploadTask.addOnProgressListener {
//                val progress = 100 * (totalSize - inputStream.available()) / totalSize
//
//                Log.d("0-0-0-0", progress.toString())
//
////            producerScope?.offer(progress)
//            }

    } catch (e: Exception) {
        e.printStackTrace()
    }

    return@withContext uri
}

/**
 * Get Download Url of Uploaded File,
 * This url will be stored in 'vendor' collection
 * in firebase for future reference
 *
 * @param task
 * @param filepath
 */
private suspend fun getDownloadUrl(
    task: Task<UploadTask.TaskSnapshot>,
    filepath: StorageReference
): Uri? = withContext(Dispatchers.IO) {
    var uri = Uri.EMPTY

    if (task.isSuccessful) { //TODO : Task failed

        try {
            uri = Tasks.await(filepath.downloadUrl)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }
    return@withContext uri
}


object Photo {
    private var circularProgressDrawable: CircularProgressDrawable? = null
    private var photo: String? = empty
    private var default = R.drawable.ic_launcher_foreground
    private var placeholder: Int? = null
    private var context: Context? = null

    /**
     * Context
     */
    fun with(context: Context?, circularDrawable: Boolean = true): Photo {
        context ?: return this

        this.context = context

        if (!circularDrawable) return this

        circularProgressDrawable = CircularProgressDrawable(context)
        circularProgressDrawable?.strokeWidth = 5f
        circularProgressDrawable?.centerRadius = 30f
        circularProgressDrawable?.start()

        return this
    }

    /**
     * Load photo
     */
    fun load(photo: String?): Photo {
        this.photo = photo
        return this
    }

    /**
     * Into
     */
    fun into(frame: ImageView, callback: (Boolean) -> Unit = {}) {
        context ?: return

        try {
            Glide.with(context!!)
                .load(photo ?: default)
                .placeholder(this.circularProgressDrawable)
                .listener(listener(callback))
                .transition(DrawableTransitionOptions.withCrossFade())
                .error(placeholder ?: default)
                .into(frame)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}

/**
 * Photo load complete listener
 */
fun listener(callback: (Boolean) -> Unit) = object : RequestListener<Drawable> {

    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
        callback(false)
        return false
    }

    override fun onResourceReady(
        resource: Drawable,
        model: Any?,
        target: Target<Drawable>,
        dataSource: DataSource?,
        isFirstResource: Boolean
    ): Boolean {
        target.onResourceReady(
            resource,
            DrawableCrossFadeTransition(300, isFirstResource)
        )
        callback(true)
        return true
    }
}