package extensions

import android.widget.Toast
import applicationContext

/**
 * Short toast
 */
fun String?.shortToast() {
    this ?: return

    Toast.makeText(applicationContext, this, Toast.LENGTH_SHORT).show()
}

/**
 * Long toast
 */
fun String?.longToast() {
    this ?: return

    Toast.makeText(applicationContext, this, Toast.LENGTH_LONG).show()
}