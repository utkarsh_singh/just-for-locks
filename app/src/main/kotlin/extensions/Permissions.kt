package extensions

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import androidx.core.app.ActivityCompat

/**
 * Check for External storage permission permission
 */
fun Context?.externalStoragePermission(requestCode: Int): Boolean {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

        if (this?.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) ==
            PackageManager.PERMISSION_GRANTED
        ) {
            true
        } else {
            // Show the permission request
            ActivityCompat.requestPermissions(
                this as Activity, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                requestCode
            )
            false
        }
    } else {
        true
    }
}