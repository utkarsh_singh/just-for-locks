package extensions

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

/**
 * Start a new activity
 *
 * @param clazz : Target class name
 * @param flags : Set flags
 * @param finish : finish current activity
 * @param bundle : Bundle
 */
fun Context.startActivity(
    clazz: Class<*>,
    flags: Boolean = false,
    finish: Boolean = false,
    bundle: Bundle? = Bundle()
) {
    val intent = Intent(this, clazz)

    //if flags is true, pass them in the intent
    if (flags) {
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
    }

    //Put the bundle in the intent
    intent.putExtras(bundle ?: Bundle())

    //Start New activity
    this.startActivity(intent)

    //finish is true, finish the current activity
    if (finish) {
        (this as AppCompatActivity).finish()
    }
}