package viewModel

import activities.MainActivity
import activities.RegistrationActivity
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import com.justforlocks.R
import kotlinx.coroutines.launch

/**
 * Login View Model
 */
class LoginViewModel : BaseViewModel() {

    /**
     * Redirect to other activities depending
     * upon authentication status
     */
    fun redirect() = viewModelScope.launch {
        if (FirebaseAuth.getInstance().currentUser != null) {
            _startActivityIntent.postValue(Pair(RegistrationActivity::class.java, null))
        } else {
            initFirebaseAuthUI()
        }
    }

    /**
     * Firebase auth UI
     */
    private fun initFirebaseAuthUI() {
        val phoneBuilder = AuthUI.IdpConfig.PhoneBuilder()
            .setDefaultCountryIso("IN")
            .setWhitelistedCountries(arrayListOf("+91"))
            .build()

        //Start activity for results
        _startActivityForResultIntent.postValue(
            AuthUI.getInstance()
                .createSignInIntentBuilder()
                .setAvailableProviders(
                    arrayListOf(
                        phoneBuilder
                    )
                )
                .setIsSmartLockEnabled(false)
                .setTheme(R.style.LoginTheme)
                .build()
        )
    }

    /**
     * User is successfully authenticated,
     * check the registration status.
     *
     * If the user is already registered, redirect to {MainActivity}
     */
    fun authenticate() = viewModelScope.launch {
        //Get user after authenticating from firebase
        val user = FirebaseAuth.getInstance().currentUser!!

        redirect()
    }
}