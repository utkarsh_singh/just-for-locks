package viewModel

import android.content.Context
import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseAuth
import com.justforlocks.R
import fragments.HomeFragment
import fragments.ProfileFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import stringResource

/**
 * MainActivity ViewModel
 */
class MainActivityViewModel : BaseViewModel() {

    val fragmentMap =
        hashMapOf("Home" to HomeFragment.newInstance(), "Profile" to ProfileFragment.newInstance())

    //Alert dialog negative button
    private val negativeButton: (DialogInterface, Int) -> Unit =
        { dialog: DialogInterface, _: Int -> dialog.cancel() }

    //Alert dialog positive button
    private val positiveButton: (DialogInterface, Int) -> Unit =
        { _: DialogInterface, _: Int ->
            viewModelScope.launch(Dispatchers.IO) {
                try {
                    _progressBar.postValue(true)

                    //Sign out from firebase
                    FirebaseAuth.getInstance().signOut()

                    _finish.postValue(true)
                } catch (e: Exception) {
                    _finish.postValue(false)
                    _toast.postValue(e.message)
                } finally {
                    _progressBar.postValue(false)
                }
            }
        }

    /**
     * Logout
     */
    fun logout(context: Context) =
        AlertDialog.Builder(context)
            .setMessage(stringResource(R.string.are_you_sure))
            .setCancelable(true)
            .setTitle(stringResource(R.string.logout))
            .apply {
                setPositiveButton(stringResource(R.string.logout), positiveButton)
                setNegativeButton(stringResource(R.string.cancel), negativeButton)
            }.create()
            .show()
}