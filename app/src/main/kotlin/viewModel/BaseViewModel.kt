package viewModel

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

abstract class BaseViewModel : ViewModel() {
    var _progressBar = MutableLiveData<Boolean>()
    var _toast = MutableLiveData<String>()
    var _snackBar = MutableLiveData<String>()
    var _startActivityForResultIntent = MutableLiveData<Intent>()
    var _startActivityIntent = MutableLiveData<Pair<Class<*>, Bundle?>>()
    var _finish = MutableLiveData<Boolean>()

    val startActivityForResult = _startActivityForResultIntent as LiveData<Intent>
    val startActivity = _startActivityIntent as LiveData<Pair<Class<*>, Bundle?>>
    val progressBar = _progressBar as LiveData<Boolean>
    val toast = _toast as LiveData<String>
    val snackBar = _snackBar as LiveData<String>
    var finish = _finish as LiveData<Boolean>
}