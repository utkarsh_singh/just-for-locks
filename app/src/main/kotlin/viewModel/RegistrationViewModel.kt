package viewModel

import User
import activities.MainActivity
import android.content.Intent
import android.util.Patterns
import androidx.core.net.toUri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest
import com.justforlocks.R
import empty
import extensions.uploadImage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import stringResource
import java.util.*

/**
 * Registration ViewModel
 */
class RegistrationViewModel : BaseViewModel() {

    private var name = empty
    private var email = empty
    private var image = empty
    private var _image = MutableLiveData<String>()
    private var _isRegistered = MutableLiveData<Boolean>()

    /**
     * Getters
     */
    val uploadedImage = _image as LiveData<String>
    val isRegistered = _isRegistered as LiveData<Boolean>

    init {
        //If user is registered then redirect to `MainActivity`
        _isRegistered.postValue(!User?.displayName.isNullOrBlank() && !User?.email.isNullOrBlank())
    }

    /**
     * Username
     */
    fun name(name: String) {
        this.name = name
    }

    /**
     * Email
     */
    fun email(email: String) {
        this.email = email
    }

    /**
     * Register user
     */
    fun register() = viewModelScope.launch(Dispatchers.IO) {
        if (!validate()) return@launch

        //Email update task
        val emailUpdateTask = FirebaseAuth
            .getInstance()
            .currentUser
            ?.updateEmail(this@RegistrationViewModel.email)

        //Photo and name update task
        val namePhotoTask = FirebaseAuth
            .getInstance()
            .currentUser
            ?.updateProfile(
                UserProfileChangeRequest.Builder()
                    .setDisplayName(this@RegistrationViewModel.name)
                    .setPhotoUri(image.toUri())
                    .build()
            )

        try {
            //Show progress bar
            _progressBar.postValue(true)

            //Execute tasks
            Tasks.await(Tasks.whenAll(emailUpdateTask, namePhotoTask))

            _toast.postValue(stringResource(R.string.registration_successful))

            //Start Main Activity
            _startActivityIntent.postValue(Pair(MainActivity::class.java, null))
        } catch (e: Exception) {
            //Display error message
            _toast.postValue(e.message)
        } finally {
            //Hide progress bar
            _progressBar.postValue(false)
        }
    }

    /**
     * Validate given fields
     */
    private fun validate(): Boolean {
        //Check if name is empty
        if (name.isBlank()) _toast.postValue("Name cannot be empty")

        //Check if email is empty
        if (email.isBlank()) _toast.postValue("Email cannot be empty")

        //Check if mail is valid
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) _toast.postValue("Invalid Email")

        //Check if image is uploaded
        if (image.isBlank()) _toast.postValue("Please choose profile image")

        return name.isNotBlank()
                && email.isNotBlank()
                && image.isNotBlank()
                && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    /**
     * Upload vendor image/shop image
     */
    fun uploadImage(data: Intent) = viewModelScope.launch {
        _progressBar.postValue(true)

        //Upload image to firebase storage
        image = data.uploadImage("images/${Calendar.getInstance().time}.jpg", 50).toString()

        //update ui
        _image.postValue(image)

        _progressBar.postValue(false)
    }
}