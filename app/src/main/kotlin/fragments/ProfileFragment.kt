package fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.justforlocks.R
import kotlinx.android.synthetic.main.fragment_profile.*
import viewModel.ProfileViewModel

/**
 * Profile Fragment
 */
class ProfileFragment : Fragment() {

    companion object {
        fun newInstance() = ProfileFragment()
    }

    private lateinit var viewModel: ProfileViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)

        //Initialize views
        initViews()
    }

    /**
     * Initialize views
     */
    private fun initViews() {
        val user = FirebaseAuth.getInstance().currentUser

        //Profile Image
        Glide.with(this)
            .load(user?.photoUrl)
            .into(profileImg1)

        //Set name and email
        profileName.text = user?.displayName
        profileEmail.text = user?.email
    }
}