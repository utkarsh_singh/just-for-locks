package utilities

import android.app.Activity
import android.net.Uri
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object ImageUploadUtil {

    /**
     * Upload Image
     *
     * @param activity
     * @param uri
     * @param path
     */
    suspend fun upload(
        activity: Activity,
        uri: Uri,
        path: String
    ) = withContext(Dispatchers.IO) {
        //Image Input Stream
        val inputStream = activity.contentResolver.openInputStream(uri)!!

        //Storage reference of stored image
        val filepath = FirebaseStorage.getInstance().reference.child(path)

        //Size of image uploaded
        val totalSize = inputStream.available()

        //Upload task
        val uploadTask = filepath.putStream(inputStream)

        var uri: Uri? = Uri.EMPTY
        try {
            //Upload image
            Tasks.await(uploadTask)

            //Get Download Url
            uri = getDownloadUrl(uploadTask, filepath)

        } catch (e: Exception) {
            e.printStackTrace()
        }

        return@withContext uri
    }

    /**
     * Get Download Url of Uploaded File,
     * This url will be stored in 'vendor' collection
     * in firebase for future reference
     *
     * @param task
     * @param filepath
     */
    private suspend fun getDownloadUrl(
        task: Task<UploadTask.TaskSnapshot>,
        filepath: StorageReference
    ): Uri? = withContext(Dispatchers.IO) {
        var uri = Uri.EMPTY

        if (task.isSuccessful) { //TODO : Task failed

            try {
                uri = Tasks.await(filepath.downloadUrl)
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }
        return@withContext uri
    }
}