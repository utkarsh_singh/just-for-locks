package utilities

import android.content.Context
import android.content.ContextWrapper
import android.graphics.*
import android.media.ExifInterface
import android.net.Uri
import android.provider.MediaStore
import applicationContext
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.util.*
import kotlin.math.roundToInt

/**
 * Compress Image
 *
 * @param Uri The uri of image to be compressed
 * @param quality Desired output quality of image
 *
 * @return file
 *
 * Get path by using file.absolutePath()
 */
fun Uri.compressImage(quality: Int = 80): File {

    val filePath = getRealPathFromURI(this.toString())

    var scaledBitmap: Bitmap? = null
    val options = BitmapFactory.Options()

    options.inJustDecodeBounds = true
    var bmp = BitmapFactory.decodeFile(filePath, options)
    var actualHeight = options.outHeight
    var actualWidth = options.outWidth
    var imgRatio = actualWidth.toFloat() / actualHeight.toFloat()
    val maxRatio =
        maxWidth / maxHeight
    if (actualHeight > maxHeight || actualWidth > maxWidth) {
        when {
            imgRatio < maxRatio -> {
                imgRatio = maxHeight / actualHeight
                actualWidth = (imgRatio * actualWidth) as Int
                actualHeight = maxHeight.toInt()
            }
            imgRatio > maxRatio -> {
                imgRatio = maxWidth / actualWidth
                actualHeight = (imgRatio * actualHeight) as Int
                actualWidth = maxWidth.toInt()
            }
            else -> {
                actualHeight = maxHeight.toInt()
                actualWidth = maxWidth.toInt()
            }
        }
    }
    options.inSampleSize = calculateInSampleSize(
        options,
        actualWidth,
        actualHeight
    )
    options.inJustDecodeBounds = false
    options.inDither = false
    options.inPurgeable = true
    options.inInputShareable = true
    options.inTempStorage = ByteArray(16 * 1024)
    try {
        bmp = BitmapFactory.decodeFile(filePath, options)
    } catch (exception: OutOfMemoryError) {
        exception.printStackTrace()
    }
    try {
        scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.RGB_565)
    } catch (exception: OutOfMemoryError) {
        exception.printStackTrace()
    }
    val ratioX = actualWidth / options.outWidth.toFloat()
    val ratioY = actualHeight / options.outHeight.toFloat()
    val middleX = actualWidth / 2.0f
    val middleY = actualHeight / 2.0f
    val scaleMatrix = Matrix()
    scaleMatrix.setScale(ratioX, ratioY, middleX, middleY)
    val canvas = Canvas(scaledBitmap!!)
    canvas.setMatrix(scaleMatrix)
    canvas.drawBitmap(
        bmp,
        middleX - bmp.width / 2,
        middleY - bmp.height / 2,
        Paint(Paint.FILTER_BITMAP_FLAG)
    )
    bmp.recycle()
    val exif: ExifInterface
    try {
        exif = ExifInterface(filePath)
        val orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0)
        val matrix = Matrix()
        when (orientation) {
            6 -> {
                matrix.postRotate(90f)
            }
            3 -> {
                matrix.postRotate(180f)
            }
            8 -> {
                matrix.postRotate(270f)
            }
        }
        scaledBitmap = Bitmap.createBitmap(
            scaledBitmap, 0, 0,
            scaledBitmap.width,
            scaledBitmap.height, matrix,
            true
        )
    } catch (e: IOException) {
        e.printStackTrace()
    }
    val out: FileOutputStream?
    val file = file()
    try {
        out = FileOutputStream(file.absolutePath)

        //write the compressed bitmap at the destination specified by filename.
        scaledBitmap!!.compress(Bitmap.CompressFormat.JPEG, quality, out)
    } catch (e: FileNotFoundException) {
        e.printStackTrace()
    }
    return file
}

//// Create the storage directory if it does not exist
//private val filename: String
//    get() {
//        val mediaStorageDir = File(
//            Environment.getExternalStorageDirectory()
//                .toString() + "/Android/data/"
//                    + applicationContext.packageName
//                    + "/Files/Compressed"
//        )
//
//        // Create the storage directory if it does not exist
//        if (!mediaStorageDir.exists()) {
//            mediaStorageDir.mkdirs()
//        }
//        val mImageName =
//            "IMG_" + System.currentTimeMillis().toString() + ".jpg"
//        return mediaStorageDir.absolutePath + "/" + mImageName
//    }

/**
 * Get Filename
 */
private val file = {
    //Create "Images" directory on local device storage
    val file: File = ContextWrapper(applicationContext).getDir("images", Context.MODE_PRIVATE)

    //Create unique filename for image
    File(file, Calendar.getInstance().time.toString() + ".jpg")
}

private const val maxHeight = 1280.0f
private const val maxWidth = 1280.0f

private fun calculateInSampleSize(
    options: BitmapFactory.Options,
    reqWidth: Int,
    reqHeight: Int
): Int {
    val height = options.outHeight
    val width = options.outWidth
    var inSampleSize = 1
    if (height > reqHeight || width > reqWidth) {
        val heightRatio =
            (height.toFloat() / reqHeight.toFloat()).roundToInt()
        val widthRatio =
            (width.toFloat() / reqWidth.toFloat()).roundToInt()
        inSampleSize = heightRatio.coerceAtMost(widthRatio)
    }
    val totalPixels = width * height.toFloat()
    val totalReqPixelsCap = reqWidth * reqHeight * 2.toFloat()
    while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
        inSampleSize++
    }
    return inSampleSize
}

private fun getRealPathFromURI(contentURI: String): String {
    val contentUri = Uri.parse(contentURI)
    val cursor = applicationContext.contentResolver.query(contentUri, null, null, null, null)
    return if (cursor == null) {
        contentUri.path!!
    } else {
        cursor.moveToFirst()
        val index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
        cursor.getString(index)
    }
}