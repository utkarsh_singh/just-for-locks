package activities

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.justforlocks.CROP_IMAGE_REQUEST_CODE
import com.justforlocks.R
import extensions.*
import kotlinx.android.synthetic.main.activity_registration.*
import viewModel.RegistrationViewModel

/**
 * Registration Activity
 */
class RegistrationActivity : AppCompatActivity() {

    private lateinit var viewModel: RegistrationViewModel

    companion object {
        const val EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE = 400
    }

    /**
     * onCreate
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
        viewModel = ViewModelProvider(this).get(RegistrationViewModel::class.java)

        //Initialize Event listeners
        initEventListener()

        //Observe Live data
        observe()
    }

    /**
     * Initialize event listener
     */
    private fun initEventListener() {
        //name
        nameET.addTextChangedListener { viewModel.name(it.toString()) }

        //email
        emailET.addTextChangedListener { viewModel.email(it.toString()) }

        //register button
        registerBtn.setOnClickListener { viewModel.register() }

        //Image click listener
        profileImg.setOnClickListener {
            if (!externalStoragePermission(EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE)) return@setOnClickListener
            pickImage()
        }
    }

    /**
     * Observe Live Data
     */
    private fun observe() {
        //Toast
        viewModel.toast.observe(this, Observer { it.shortToast() })

        //Start activity
        viewModel.startActivity.observe(this, Observer {
            startActivity(it.first, true, true, it.second)
            finish()
        })

        //Uploaded image
        viewModel.uploadedImage.observe(this, Observer {
            Photo.with(this)
                .load(it)
                .into(profileImg) {}
        })

        //Progress Bar
        viewModel.progressBar.observe(this, Observer {
            registrationProgressBar.visibility = if (it) View.VISIBLE else View.INVISIBLE
        })

        //Is user registered
        viewModel.isRegistered.observe(this, Observer {
            if (it) {
                startActivity(MainActivity::class.java, true, true)
                finish()
            } else {
                registrationRL.visibility = View.GONE
            }
        })
    }

    /**
     * Default method
     */
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode != EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE) return

        //Pick image if permission is granted
        if (grantResults.first() == PackageManager.PERMISSION_GRANTED)
            pickImage() //Pick image from external storage
    }

    /**
     * Default method
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != Activity.RESULT_OK) return

        //If Request code is {CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE}, then upload the image
        if (requestCode == CROP_IMAGE_REQUEST_CODE) {
            viewModel.uploadImage(data!!)
        }
    }
}