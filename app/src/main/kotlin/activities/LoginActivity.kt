package activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.firebase.ui.auth.IdpResponse
import com.justforlocks.R
import extensions.startActivity
import viewModel.LoginViewModel

/**
 * Login Activity
 */
class LoginActivity : AppCompatActivity() {
    private lateinit var viewModel: LoginViewModel

    companion object {
        private const val RC_SIGN_IN = 123
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        //Initialize View model
        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)

        //Redirect to MainActivity/AuthActivity
        viewModel.redirect()

        //Observe Live Data
        observe()
    }

    /**
     * Observe Live Data
     */
    private fun observe() {
        //Start activity for result
        viewModel.startActivityForResult.observe(this, Observer { startActivityForResult(it, RC_SIGN_IN) })

        //Star activity
        viewModel.startActivity.observe(this, Observer { this.startActivity(it.first, true, true, it.second) })
    }

    /**
     * On Activity Result
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            val response = IdpResponse.fromResultIntent(data)

            if (resultCode == Activity.RESULT_OK) {
                viewModel.authenticate()
            } else {
                finish()
            }
        }
    }
}