package activities

import adapters.ViewPagerAdapter
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.widget.ViewPager2
import com.justforlocks.R
import extensions.setupWithViewPager2
import extensions.shortToast
import extensions.startActivity
import kotlinx.android.synthetic.main.activity_main.*
import viewModel.MainActivityViewModel

/**
 * MainActivity
 */
class MainActivity : AppCompatActivity() {
    private lateinit var viewModel: MainActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProvider(this).get(MainActivityViewModel::class.java)

        //Initialize views
        initViews()

        //Initialize event listeners
        initEventListeners()

        //Observe live data
        observe()
    }

    /**
     * Initialize views
     */
    private fun initViews() {
        mainViewPager.adapter = ViewPagerAdapter(this, viewModel.fragmentMap.values.toList())

        viewModel.fragmentMap.keys
            .forEach {
                mainTabLayout.addTab(mainTabLayout.newTab().apply { text = it })
            }
    }

    /**
     * Initialize event listeners
     */
    private fun initEventListeners() {
        //Setup tab layout with viewpager
        mainTabLayout.setupWithViewPager2(mainViewPager, viewModel.fragmentMap.keys.toList())

        //Change tab on viewpager swipe
        mainViewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                mainTabLayout.selectTab(mainTabLayout.getTabAt(position))
            }
        })

        //Logout button
        logoutButton.setOnClickListener {
            viewModel.logout(this)
        }
    }

    /**
     * Observe live data
     */
    private fun observe() {
        //Finish
        viewModel.finish.observe(this, Observer {
            if (it) {
                startActivity(SplashScreenActivity::class.java, true, true)
                finish()
            }
        })

        //Toast
        viewModel.toast.observe(this, Observer {
            it.shortToast()
        })

        //Progress bar
        viewModel.progressBar.observe(this, Observer {
            mainActivityProgressBar.visibility = if (it) View.VISIBLE else View.INVISIBLE
        })
    }
}