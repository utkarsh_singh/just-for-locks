package activities

import android.os.Bundle
import android.os.CountDownTimer
import androidx.appcompat.app.AppCompatActivity
import com.justforlocks.R
import extensions.startActivity

/**
 * Splash Screen activity
 */
class SplashScreenActivity : AppCompatActivity() {
    /**
     * onCreate
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
    }

    /**
     * onStart
     */
    override fun onStart() {
        super.onStart()

        //Redirect
        redirect()
    }

    /**
     * Redirect to main activity if logged in,
     * else redirect to login activity
     */
    private fun redirect() = object : CountDownTimer(1500, 500) {
        override fun onFinish() {
            this@SplashScreenActivity.startActivity(LoginActivity::class.java, true, true)
            finish()
        }

        override fun onTick(p0: Long) {
        }
    }.start()
}